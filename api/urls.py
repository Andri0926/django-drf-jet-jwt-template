from django.conf.urls import url
from django.urls.conf import include
from django.urls import path
from rest_framework import routers
from dj_rest_auth.registration.views import VerifyEmailView, ConfirmEmailView
from dj_rest_auth.views import PasswordResetConfirmView


router = routers.DefaultRouter()

urlpatterns = [
    url('', include(router.urls)),
    url('auth/', include('dj_rest_auth.urls')),
    
    path(
        'auth/registration/account-confirm-email/<str:key>/',
        ConfirmEmailView.as_view(),
    ), # Needs to be defined before the registration path
    
    path('auth/registration/', include('dj_rest_auth.registration.urls')),
    
    path('auth/account-confirm-email/', VerifyEmailView.as_view(), name='account_email_verification_sent'),
    
    path(
        'rest-auth/password/reset/confirm/<slug:uidb64>/<slug:token>/',
        PasswordResetConfirmView.as_view(), name='password_reset_confirm'
    ),

]